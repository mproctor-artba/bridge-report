@extends('layouts.right-aside')

@section('css')
	<style type="text/css">
	@media only screen and (min-width: 501px){
		.row .vertical-align p.header-caption{
			max-width: 80%;
			margin: 30% auto 0 auto;
			display: block;
			font-size: 24px;
		}
	}
	</style>
@endsection

@section('content')
    
<div class='col-md-12 text-center'>
    <h1 class='page-title'>Bridge Report</h1>
    <p class="header-caption">There are 168.5 million crossings on nearly 42,100 bridges rated in poor condition.</p>
</div>
<div class='col-md-6 text-center'>
	<a href="/state/map">
    	<img src="/img/state-map.jpg?i=10">
    </a>
    <br>
    <h3><a href="/state/map">State Map</a></h3>
</div>
<div class='col-md-6 text-center'>
	<a href="/congressional/map">
    <img src="/img/congressional-map.jpg?i=10">
    </a>
    <br>
    <h3><a href="/congressional/map">Congressional Map</a></h3>
</div>
<div class="col-md-12 text-center">
	<h2>1 in 3 U.S. Bridges Needs Repair or Replacement...</h2>
	<p>New Federal Bridge Formula Program Provides Critical Resources<br><br>

<a href="/reports/{{ env('CURRENT_YEAR') }}-ARTBA-Bridge-Report.pdf" target="_blank" class="btn btn-danger btn-flat" style="width: 300px; margin: auto;">View Full Report</a>

	</p>

	<div class="row">
		<div class="col-md-6 vertical-align text-left">
		
			<p class="header-caption text-center">As of July 2024, states have committed 46 percent of the new bridge formula funds currently available through year two of the Infrastructure Investment and Jobs Act (IIJA).<br><br>
			<!--
			<span class="caption-body text-left">Voters in 31 states Nov. 6 showed their support for transportation construction investment , approving 79 percent of 352 state and local ballot measures</span>-->
			</p>
			
		</div>
		<div class="col-md-6"><img src="/img/graph_one.jpg"></div>
	</div>
	<div class="row">
		<div class="col-md-6"><img src="/img/graph_two.jpg"></div>
				<div class="col-md-6 vertical-align">
			<p class="header-caption text-center">1 out of 3 U.S. bridges still needs to be replaced or repaired, even as the nation’s share of bridges in poor condition continues to slowly improve.<br>
			<!--<span class="caption-body text-left">Voters in 31 states Nov. 6 showed their support for transportation construction investment , approving 79 percent of 352 state and local ballot measures</span>--></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 vertical-align"><p class="header-caption text-center">In addition to bridge replacements, states have identified major rehabilitation, deck repair, and bridge widening projects that will improve safety and conditions.</p></div>
		<div class="col-md-6"><img src="/img/graph_three.jpg"></div>
	</div>
	<!--
	<div class="row">
		<div class="col-md-6">
			<iframe width="100%" height="400" src="https://www.youtube.com/embed/-ZrwuthvTB0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
		<div class="col-md-6 vertical-align">
			<p class="header-caption text-center">The 2022 ARTBA Bridge Report with Dr. Alison Black</p>
		</div>
	</div>
	-->
</div>
@endsection

@section('right-side')
	<h3 class="text-center" style="margin-bottom: 0;">State Ranking</h3>
	<p class="text-center">in % of structurally deficient bridges</p>
	<table class="table" style="margin-bottom: 0; max-width: 436px; width: 90%;">
		<thead>
			<tr>
				<th style="width: 80%">State</th>
				<th class="text-center">Rank</th>
			</tr>
		</thead>
	</table>
	<div class="" style="max-height: 300px; overflow-y: auto;">
		<table class="table" style="margin-bottom: 0;">
			<tbody >
				@foreach($states as $state)
					<tr>
						<td style="width:80%">
							<a href="/state/profile/{{ $state->state_abbr }}">{{ $state->state }}</a>
						</td>
						<td class="text-center">
							{{ $state->percent_rank }}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<br>
	<a href="{{ route('state-ranking') }}" class="btn btn-danger btn-flat btn-full-width">View Full Ranking</a>
	@include('layouts.acrow-ad')
@endsection

@section('js')

@endsection