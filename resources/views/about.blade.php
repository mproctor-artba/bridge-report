@extends('layouts.dual-aside')

@section('content')
    
<div class='col-md-12 text-left'>
    <h1 class='page-title'>About the Report</h1>
    <p>
    	ARTBA’s Annual Bridge Report is a comprehensive analysis of the condition of bridges in the United States. Using this report, you can:
    </p>
    <ul>
		<li>View the condition of bridges in your state and congressional district</li>
		<li>See how your state stacks up compared to other states, and how that ranking has changed over time</li>
		<li>Find the top deficient bridges in the country, as well as in your state and congressional district</li>
	</ul>
	<p>
Federal law requires that most bridges in the United States be regularly inspected by the state departments of transportation for functionality and structural condition. The states report this information—and much more—on each bridge under their jurisdiction to the U.S. Department of Transportation (U.S. DOT) for inclusion in the National Bridge Inventory (NBI).
</p>
<p>
Unless indicated otherwise, all of the information available in ARTBA’s Bridge Report comes from the {{ env('CURRENT_YEAR') }} NBI and is collected and managed by the U.S. DOT’s Federal Highway Administration (FHWA). The information is updated at least once a year. As such, it is possible the status and condition on a particular bridge may have changed since the data were collected. For absolute surety on the current status and condition of a specific bridge, please contact the relevant state and/or local transportation department. The current NBI data was downloaded on <strong>August 20, {{ env('CURRENT_YEAR') }}</strong>.
</p>
<p>
The government classifies a bridge as “structurally deficient” if any one of the following bridge components are rated less than or equal to 4 (in poor or worse condition):
</p>
<ul>
	<li>Deck condition</li>
	<li>Superstructure condition</li>
	<li>Substructure condition</li>
	<lu>Culvert condition</li>
</ul>
<p>
The FHWA uses four factors in determining the “sufficiency rating” of a bridge, which is its sufficiency to remain in service. Each bridge is assigned a numerical percentage rating between zero and 100, with 100 being the ideal.
</p>
<p>
<strong>Effective Jan. 1, 2018, FHWA changed the definition of structurally deficient</strong> as part of the final rule on highway and bridge performance measures, published May 20, 2017, pursuant to the 2012 surface transportation law Moving Ahead for Progress in the 21st Century Act (MAP-21). Two measures that were previously used to classify bridges as structurally deficient are no longer used. This includes bridges where the overall structural evaluation was rated in poor or worse condition, or with insufficient waterway openings. The new definition limits the classification to bridges where one of the key structural elements—the deck, superstructure, substructure or culverts—are rated in poor or worse condition. 
    </p>

    <h2>About ARTBA</h2>
    <p>
The Washington, D.C.-based American Road & Transportation Builders Association (ARTBA) is a non-partisan federation whose primary goal is to aggressively grow and protect transportation infrastructure investment to meet the public and business demand for safe and efficient travel. ARTBA was established in 1902 by Michigan public official Horatio Earle with this express purpose: to advocate for construction of a federally-led “Capital Connecting Government Highway” that he said would connect “every state capital with every other state capital, and every capital with the United States Capital-Washington.” Mission accomplished in 1956.
    </p>
</div>

@endsection

@section('right-side')
	<h3 class="text-center" style="margin-bottom: 0;">Member Benefits</h3>

	<p>ARTBA’s communications platform and its exclusive focus on transportation keeps its members up-to-speed and provides analysis in real time on policy, regulatory, legal and safety developments from Washington, D.C. ARTBA’s economic research and analyses also provide members with the market intelligence they need to make smart business decisions</p>
	
	<ul>
		<li>Bridge Inventory</li>
		<li>Discounted Industry Forecasts</li>
	</ul>
	
	<a href="https://www.artba.org/artbamembers/" class="btn btn-danger btn-flat btn-full-width">Become a Member</a>

	<br>
							<p class="text-center"><small>Advertisement</small></p>
							<!-- /22854919040/bridgereport -->
							<div id='div-gpt-ad-1670950508715-0' style='min-width: 350px; min-height: 350px;'>
							  <script>
							    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1670950508715-0'); });
							  </script>
							</div>

@endsection

@section('js')
	<script>
	$(document).ready(function(){
		if($( window ).width() > 700){
	    	$("#right-aside").sticky({topSpacing:0});
		}
	  });
	}
	</script>
@endsection