<div class="card card-silver" style="max-width:400px;">
	<div class="card-header text-center">
		<h2 class="text-center card-title slim-margins"><span class="numbercounter">{{ $rankings->num_rank }}</span> <br> <small class="text-small"><i class=""></i> Compared to {{ NumRankOld($rankings->state) }} in {{ env('PAST_YEAR') }}</small></h2>
		<p>in the nation in # of structurally deficient bridges</p>
	</div>
	<div class="card-body">
		<table class="table table-striped light">
			@if($rankings->num_rank > 1)
			<thead>
				<tr>
					<th><a href="/state/profile/{{ $best_numrank->state_abbr }}">1. {{ $best_numrank->state }}</a></th>
					<th class="text-center">{{ number_format($best_numrank->num_deficient) }}</th>
				</tr>
			</thead>
			@endif
			<tbody>
			@if($rankings->num_rank > 1 && $defnum_neighborUp->num_rank > 1)
				<tr>
					<td><a href="/state/profile/{{ $defnum_neighborUp->state_abbr }}">{{ $defnum_neighborUp->num_rank }}. {{ $defnum_neighborUp->state }}</a></td>
					<td class="text-center">{{ number_format($defnum_neighborUp->num_deficient) }}</td>
				</tr>
			@endif
				<tr class="active">
					<td>{{ $rankings->num_rank }}. {{ $text->stname }}</td>
					<td class="text-center">{{ number_format($rankings->num_deficient) }}</td>
				</tr>
				@if($rankings->num_rank < 51)
				<tr>
					<td><a href="/state/profile/{{ $defnum_neighborDown->state_abbr }}">{{ $defnum_neighborDown->num_rank }}. {{ $defnum_neighborDown->state }}</a></td>
					<td class="text-center">{{ number_format($defnum_neighborDown->num_deficient) }}</td>
				</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>