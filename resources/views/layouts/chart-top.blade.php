@php 
	$chartBottomYears = [];
    $currentYear = env('CURRENT_YEAR');

	for($i = env('CURRENT_YEAR') - 4; $i <= env('CURRENT_YEAR'); $i++){
		$chartBottomYears[] = $i;
	}

	$yearsString = implode('","', $chartBottomYears);

@endphp

var barChartData = {
	labels: ["{!! $yearsString !!}"],
	datasets: [{
		label: 'Bridges in Need of Repair, Including SD Bridges',
		backgroundColor: "{{ env('MAP_H1') }}",
		borderColor: "{{ env('MAP_H1') }}",
		borderWidth: 1,
		data: [
			@foreach($chartonevalues as $chartonevalue)
				{{ $chartonevalue->rep_count }},
			@endforeach
		]
	}]

};