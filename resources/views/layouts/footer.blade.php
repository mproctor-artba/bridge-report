<footer class="row footer pdfcrowd-remove">
    <div class="col-md-12">
        <div class="col-lg-10 col-lg-offset-1" style="padding-top: 25px;">
            <div class="col-md-9 col-sm-12">
            </div>
            <!--
            <div class="col-md-3 col-sm-12 text-right">
                <a href="/news/transportation-builder/"><img src="http://www.artba.artba.co/wp-content/themes/Avada/home/img/tb.png" width="200px"></a>
            </div>
            -->
            <div class="col-md-8">
                <ul>
                    <li><br><h6><a href="https://artbabridgereport.org"><img src="https://artbabridgereport.org/img/ARTBA-logo@2x-white.png" width="200px"></a></h6></li>
                    <li><p>Source: Data is from the Federal Highway Administration (FHWA) National Bridge Inventory (NBI), downloaded on August 20, 2024. Note that specific conditions on bridges may have changed as a result of recent work or updated inspections.</p>ARTBA is a non-partisan federation whose primary goal is to aggressively grow and protect transportation infrastructure investment to meet the public and business demand for safe and efficient travel.</p></li>
                    <li><a href="https://www.artba.org/privacy-policy">Privacy &amp; Cookies Policy</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <h6><strong>Connect With Us</strong></h6>
                <ul>
                    <li><a href="https://www.facebook.com/ARTBAssociation/" target="_blank" class="social"><i class="fa fa-facebook"></i></a> <a href="https://www.linkedin.com/company/110745/" class="social"><i class="fa fa-linkedin"></i></a> <a href="https://twitter.com/ARTBA" class="social"><i class="fa fa-twitter"></i></a></li>
                    <li style="margin-top: 10px;">
                    </li>
                </ul>
            </div>
            <div class="col-md-12 text-center">
                <p>Copyright © {{ Date('Y') }} American Road &amp; Transportation Builders Association</p>
            </div>
        </div>
    </div>
</footer>