@php 
	$chartBottomYears = [];
    $currentYear = env('CURRENT_YEAR');

	for($i = env('CURRENT_YEAR') - 4; $i <= env('CURRENT_YEAR'); $i++){
		$chartBottomYears[] = $i;
	}

	$yearsString = implode('","', $chartBottomYears);

@endphp

var chartData = {
			labels: ["{!! $yearsString !!}"],
			datasets: [{
				id: 'A',
				yAxisID: 'A',
				type: 'line',
				label: '% of All Bridges Classified as Structurally Deficient',
				borderColor: "{{ env('MAP_M1') }}",
				borderWidth: 2,
				fill: "{{ env('MAP_M1') }}",
				backgroundColor: "{{ env('MAP_M1') }}",
				data: [
					@foreach($charttwovalues as $charttwovalue)
						{{ number_format($charttwovalue->percent_def,1) }},
					@endforeach
				]
			}, {
				id: 'B',
				yAxisID: 'B',
				type: 'bar',
				label: 'Structurally Deficient Bridges',
				backgroundColor: "{{ env('MAP_L1') }}",
				data: [
					@foreach($charttwovalues as $charttwovalue)
						{{ $charttwovalue->def }},
					@endforeach
				]
			}],
			  options: {
			    
			  }

		};