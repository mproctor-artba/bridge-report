<!DOCTYPE HTML>
<html class="full-height">
	<head>
		<link rel="icon" href="/img/favicon.png">
		<meta http-equiv='X-UA-Compatible' content='IE=edge' />
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
		<meta name='viewport' content='width=device-width, initial-scale=1' />
		<meta name="description" content="ARTBA’s annual Bridge Report is a comprehensive analysis of the condition of bridges in the United States">
		<title>{{ $title ?? "ARTBA Bridge Report" }}</title>
		<meta property="og:url" content="https://artbabridgereport.org" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="ARTBA Bridge Report" />
		<meta property="og:description" content="ARTBA’s annual Bridge Report is a comprehensive analysis of the condition of bridges in the United States" />
		<meta property="og:image" content="https://artbabridgereport.org/img/bridgereportsocialbanner.jpg?i=2" />
		<link rel='stylesheet' id='avada-stylesheet-css'  href='/Avada/assets/css/style.min.css?ver=5.1.6' type='text/css' media='all' />
		<link href='https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css' rel='stylesheet'>
		<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>
		<link href='https://designmodo.github.io/Flat-UI/dist/css/flat-ui.min.css' rel='stylesheet'>
		<link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' rel='stylesheet'>
		<link href='/css/theme.css' rel='stylesheet'>
		<link href="/css/artba-2019.css" rel='stylesheet'>
		<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-WD69Z88');</script>
		<!-- End Google Tag Manager -->
		<!-- ADSENSE -->
			<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
			<script>
			  window.googletag = window.googletag || {cmd: []};
			  googletag.cmd.push(function() {
			    googletag.defineSlot('/22854919040/bridgereport', [350, 350], 'div-gpt-ad-1670950508715-0').addService(googletag.pubads());
			    googletag.pubads().enableSingleRequest();
			    googletag.enableServices();
			  });
			</script>
		<!-- END ADSENSE -->
		@yield('css')
		<style type="text/css">
			
		</style>
	</head>
	<body class='home page-template page-template-home page-template-index page-template-homeindex-php page page-id-18227 tribe-no-js fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text'>
		@include('layouts.nav')
		<div class='full-width-container full-height'>
			<section class='fusion-fullwidth full-height'>
				<article class='full-height'>
					<!-- PARALLAX TITLE BAR-->
						<div class='pdfcrowd-remove parallax-title fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-fixed nonhundred-percent-fullwidth' style='background-image: url("https://www.artbabridgereport.org/img/general_banner.jpg"); background-size:cover; background-repeat:no-repeat; background-attachment: fixed; background-size: 100% 250px;'>
							<div class='col-md-7 col-md-offset-2'>
							</div>
						</div>
					<!-- END TITLE BAR -->
					<!-- WHERE THE FUN STARTS -->
					<div id='page-content' class='full-height'>
					<div class='row full-height'>
					<!--
						<div class='col-md-2 aside full-height no-padding'>
							<aside class='aside-dark box-shadow-2 full-height'>
							@yield('aside')
							</aside>
						</div>
-->
						<div class='col-md-8 col-md-offset-1 box-shadow-2 full-height raise bg-white'>
							@yield('content')
						</div>
						<div class='col-md-3 aside aside-right-fixed right-content' id="right-aside">
							
								@yield('right-side')

						</div>
					</div>
					</div>
				</article>
				<!-- FOOTER -->
					@include('layouts.footer')
				<!-- END FOOTER -->
			</section>
			</div>
			@include('layouts.js')
	</body>
</html>