<div class="card card-silver" style="max-width:400px;">
	<div class="card-header text-center">
		<h2 class="card-title slim-margins"><span class="numbercounter">{{ $rankings->percent_rank }}</span> <br> <small class="text-small"><i class=""></i> Compared to {{ PercentRankOld($rankings->state) }} in {{ env('PAST_YEAR') }}</small></h2>
		<p>in the nation in % of structurally deficient bridges</p>
	</div>
	<div class="card-body">
		<table class="table table-striped light">
		@if($rankings->percent_rank > 1)
			<thead>
				<tr>
					<th><a href="/state/profile/{{ $best_percent_rank->state_abbr }}">1. {{ $best_percent_rank->state }}</a></th>
					<th class="text-center">{{ number_format($best_percent_rank->percent_deficient, 1) }}%</th>
				</tr>
			</thead>
		@endif
			<tbody>
				@if($rankings->percent_rank > 1 && $defnum_neighborUp != null && $defnum_neighborUp->percent_rank > 1)
				<tr>
					<td><a href="/state/profile/{{ $defpercent_neighborUp->state_abbr }}">{{ $defpercent_neighborUp->percent_rank }}. {{ $defpercent_neighborUp->state }}</a></td>
					<td class="text-center">{{ number_format($defpercent_neighborUp->percent_deficient, 1) }}%</td>
				</tr>
				@endif
				<tr class="active">
					<td>{{ $rankings->percent_rank }}. {{ $text->stname }}</td>
					<td class="text-center">{{ number_format($rankings->percent_deficient, 1) }}%</td>
				</tr>
				@if($rankings->percent_rank < 51)
				<tr>
					<td><a href="/state/profile/{{ $defpercent_neighborDown->state_abbr }}">{{ $defpercent_neighborDown->percent_rank }}. {{ $defpercent_neighborDown->state }}</a></td>
					<td class="text-center">{{ number_format($defpercent_neighborDown->percent_deficient, 1) }}%</td>
				</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>