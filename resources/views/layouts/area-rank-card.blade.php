<div class="card card-silver" style="max-width:400px;">
	<div class="card-header text-center">
		<h2 class="card-title slim-margins"><span class="numbercounter">{{ $rankings->area_rank }}</span> <br> <small class="text-small"><i class=""></i>Compared to {{ AreaRankOld($rankings->state) }} in {{ env('PAST_YEAR') }}</small></h2>
		<p>in the nation in % of structurally deficient bridge deck area</p>
	</div>
	<div class="card-body">
		<table class="table table-striped light">
		@if($rankings->area_rank > 1)
			<thead>
				<tr>
					<th><a href="/state/profile/{{ $best_area_rank->state_abbr }}">1. {{ $best_area_rank->state }}</a></th>
					<th class="text-center">{{ number_format($best_area_rank->pcnt_area, 1) }}%</th>
				</tr>
			</thead>
		@endif
			<tbody>
				@if($rankings->area_rank > 1 && $defnum_neighborUp != null && $defnum_neighborUp->area_rank > 1)
				<tr>
					<td><a href="/state/profile/{{ $defarea_neighborUp->state_abbr }}">{{ $defarea_neighborUp->area_rank }}. {{ $defarea_neighborUp->state }}</a></td>
					<td class="text-center">{{ number_format($defarea_neighborUp->pcnt_area, 1) }}%</td>
				</tr>
				@endif
				<tr class="active">
					<td>{{ $rankings->area_rank }}. {{ $text->stname }}</td>
					<td class="text-center">{{ number_format($rankings->pcnt_area, 1) }}%</td>
				</tr>
				@if($rankings->area_rank < 51)
				<tr>
					<td><a href="/state/profile/{{ $defarea_neighborDown->state_abbr }}">{{ $defarea_neighborDown->area_rank }}. {{ $defarea_neighborDown->state }}</a></td>
					<td class="text-center">{{ number_format($defarea_neighborDown->pcnt_area, 1) }}%</td>
				</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>