@extends('layouts.full-width')

@section('css')
	<style type="text/css">
		#table-head-sticky-wrapper{
			display: contents;
		}
		#datatable-buttons_filter{
			margin: 0 auto;
			width: 100%;
		}
		#datatable-buttons_filter label{
			width: 100%;
		}
		#datatable-buttons_filter input{
			width: 100%;
			max-width: 300px;
			display: inline;
			margin-left:20px;
		}
	</style>
@endsection

@section('content')

<div class='col-md-9 text-center'>
    <h1 class='page-title'>State Rankings</h1>

	
<div id="table-search"></div>
</div>
<div class="col-md-3" style="background: whitesmoke; border-radius: 7px; padding:15px; margin: 20px 0;">
	<!--
	<a href="/reports/ARTBA 2020 Bridge Report - State Ranking.pdf" target="_blank" class="btn btn-danger btn-flat text-white" style="max-width: 200px; align-content: center; margin:auto;">Download Full PDF</a>
-->
<br><select id="yearquickselect" class="form-control" style="width:100%; max-width: 400px; margin: 0px auto;">
	<option value="{{ env('CURRENT_YEAR') }}">{{ env('CURRENT_YEAR') }} Data</option>
	@for($i = env('CURRENT_YEAR') - 1; $i >= (env('CURRENT_YEAR') - 4); $i--)
		<option value="{{ $i }}">{{ $i }} Data</option>
	@endfor
</select>
</div>
<div class='col-md-12 text-center'>

	 <table id="datatable-buttons" class="display table-condensed table table-hover table-bordered dt-responsive responsive-display">
        <thead id="table-head">
        <tr style="background-color: #005583">
			<th id="th-buttons" class="dt-buttons table-header-buttons" colspan="8" rowspan="1"></th>
		</tr>
            <tr>
            	<th class="text-left" style="max-width: 100px;"><span class="currentYear">{{ env('CURRENT_YEAR') }}</span> State Data</th>
                <th class="text-center" style="max-width: 100px;">Rank: SD bridges as % of inventory</th>
                <th class="text-center" style="max-width: 100px;">Rank: # SD bridges</th>
                <th class="text-center" style="max-width: 100px;">Rank: % of bridge area SD</th>
                <th class="text-center" style="max-width: 100px;">Number of Bridges</th>
                <th class="text-center" style="max-width: 100px;">Number of SD bridges</th>
                <th class="text-center" style="max-width: 100px;">SD Bridges as % of Inventory</th>
                <th class="text-center" style="max-width: 100px;">SD bridge area as % of total area</th>
            </tr>
        </thead>
        <tbody id="state-ranking"></tbody>
    </table>
</div>
<div class="col-md-12 text-center">

</div>

@endsection

@section('right-side')
	@include('layouts.acrow-ad')
@endsection

@section('js')
	<script src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.1/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
    	$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': "{{ csrf_token() }}"
	          }
	        });

	    	var table = $('#datatable-buttons').DataTable({
			responsive: true,
			paging: false,
			"order": [[ 1, "asc" ]],
	  		ajax: {
		    	"url":"/state/rankings",
		    	"type": "POST",
		    	"data": function (d){
		     		d.year = $("#yearquickselect").val()
		    }
		  },
			columns: [
			{ "data": "state", "class" : "text-left"},
			{ "data": "percent_rank"},
			{ "data": "num_rank"},
			{ "data": "area_rank"},
		    { "data": "bridges"},
		    { "data": "num_deficient"},
		    { "data": "percent_deficient"},
		    { "data": "pcnt_area"}
		  ],
		});

		new $.fn.dataTable.Buttons(table, {
			// data-spy="afix" data-offset-top="#navbar"
			buttons: [
				'pdf', 'excel','csv'
			],

		});

		table.buttons(0, null).container().prependTo(
			$('.table-header-buttons:eq(0)'), table.table().container()
		);
		
		if($( window ).width() > 700){
			$("#right-aside").sticky({topSpacing:0});
		}

	    $("#yearquickselect").on("change", function(){
	    	var year = $(this).val();
	    	var table = $('#datatable-buttons').DataTable();
	    	/*
	    	$().each(function(){

	    	})
	    	*/
	    	$(".currentYear").text(year);

	    	table.clear();
	    	table.ajax.reload();
	    });

	    $("#datatable-buttons_filter").appendTo("#table-search");
	});
	</script>
@endsection