@extends('layouts.dual-aside')

@section('css')
<style type="text/css">
    img.doc-icon{
        height: 150px;
        margin: 0 auto;
    }
    .doc-title{
        min-height: 62px;
    }
    .doc-title strong{
        font-size: 15px;
    }
</style>
@endsection

@section('content')
    
<div class='col-md-12 text-left'>
    <h1 class='page-title'>The Rehabilitated Arlington Memorial Bridge</h1>
    <div class="row mb20">
        <div class="col-md-12">
            <iframe width="100%" height="500" src="https://www.youtube.com/embed/_oLQ3BbL-P8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
    <div class="row mb20" style="min-height: 300px;">
        <div class="col-md-12">
            <h3></h3>
            <p>The three-minute story of the newly rehabilitated Arlington Memorial Bridge in Washington, D.C.</p>
            <br>
            <!--
            <a href="#" class="btn" target="_blank" style="background: #16a085; color:white !important;">Take Action</a>
            -->
        </div>
    </div>
</div>

@endsection

@section('right-side')
    <h3 class="text-center" style="margin-bottom: 0;">Member Benefits</h3>

    <p>ARTBA’s communications platform and its exclusive focus on transportation keeps its members up-to-speed and provides analysis in real time on policy, regulatory, legal and safety developments from Washington, D.C. ARTBA’s economic research and analyses also provide members with the market intelligence they need to make smart business decisions</p>
    
    <ul>
        <li>Bridge Inventory</li>
        <li>Discounted Industry Forecasts</li>
    </ul>
    
    <a href="https://www.artba.org/artbamembers/" class="btn btn-danger btn-flat btn-full-width">Become a Member</a>
    @include('layouts.acrow-ad')
@endsection

@section('js')
    <script>
    $(document).ready(function(){
        if($( window ).width() > 700){
        $("#right-aside").sticky({topSpacing:0});
    }
      });
    </script>
@endsection