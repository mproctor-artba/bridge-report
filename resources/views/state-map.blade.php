@extends('layouts.right-aside')

@section('content')

<div class='col-md-12 text-center'>
    <h1 class='page-title'>Bridge Report</h1>
    <p class="header-caption">There are 168.5 million crossings on nearly 42,100 bridges rated in poor condition.</p>
</div>
<div class='col-md-10 col-md-offset-1 text-center'>
	<div id='map-container'></div>
	<link href="/css/map.css" rel="stylesheet">
	<script src="/js/raphael.min.js"></script>
	<script src="/js/map.js"></script>
	<script>
	 	var map_cfg = {!! $map !!}; 
		var map = new FlaMap(map_cfg);
		map.drawOnDomReady('map-container');
	</script>
	<div id="map-pr" style="display:none; position: absolute; bottom: 20%; right:10%;">
		<a href="/state/profile/PR" class="btn" style="color:white; width:50px; background-color: {{ env('MAP_H1') }};">PR</a>
	</div>
</div>
<div class="col-md-12 text-center"></div>

@endsection

@section('right-side')
	<h3 class="text-center" style="margin-bottom: 0;">State Map</h3>
<p class="text-center">Shows % of bridges structurally deficient.</p>

<table class="table nolines">
	<tbody>
		<tr>
			<td style="width:50px; background-color: {{ env('MAP_L1') }};">		</td>
			<td>0 - 4.9%</td>
		</tr>
		<tr>
			<td style="background-color: {{ env('MAP_M1') }};">		</td>
			<td>5 - 8.9%</td>
		</tr>
		<tr>
			<td style="background-color: {{ env('MAP_H1') }};">		</td>
			<td>9% or higher</td>
		</tr>
	</tbody>
</table>

<select id="statequickselect" class="form-control" style="width:100%;">
	<option value="NULL">Select State to View Report</option>
	<option value="AL">Alabama</option>
	<option value="AK">Alaska</option>
	<option value="AZ">Arizona</option>
	<option value="AR">Arkansas</option>
	<option value="CA">California</option>
	<option value="CO">Colorado</option>
	<option value="CT">Connecticut</option>
	<option value="DE">Delaware</option>
	<option value="DC">District Of Columbia</option>
	<option value="FL">Florida</option>
	<option value="GA">Georgia</option>
	<option value="HI">Hawaii</option>
	<option value="ID">Idaho</option>
	<option value="IL">Illinois</option>
	<option value="IN">Indiana</option>
	<option value="IA">Iowa</option>
	<option value="KS">Kansas</option>
	<option value="KY">Kentucky</option>
	<option value="LA">Louisiana</option>
	<option value="ME">Maine</option>
	<option value="MD">Maryland</option>
	<option value="MA">Massachusetts</option>
	<option value="MI">Michigan</option>
	<option value="MN">Minnesota</option>
	<option value="MS">Mississippi</option>
	<option value="MO">Missouri</option>
	<option value="MT">Montana</option>
	<option value="NE">Nebraska</option>
	<option value="NV">Nevada</option>
	<option value="NH">New Hampshire</option>
	<option value="NJ">New Jersey</option>
	<option value="NM">New Mexico</option>
	<option value="NY">New York</option>
	<option value="NC">North Carolina</option>
	<option value="ND">North Dakota</option>
	<option value="OH">Ohio</option>
	<option value="OK">Oklahoma</option>
	<option value="OR">Oregon</option>
	<option value="PA">Pennsylvania</option>
	<option value="PR">Puerto Rico</option>
	<option value="RI">Rhode Island</option>
	<option value="SC">South Carolina</option>
	<option value="SD">South Dakota</option>
	<option value="TN">Tennessee</option>
	<option value="TX">Texas</option>
	<option value="UT">Utah</option>
	<option value="VT">Vermont</option>
	<option value="VA">Virginia</option>
	<option value="WA">Washington</option>
	<option value="WV">West Virginia</option>
	<option value="WI">Wisconsin</option>
	<option value="WY">Wyoming</option>
</select>
<br>
<a href="{{ route('congressional-map') }}" class="btn btn-danger btn-flat btn-full-width">Switch to Congressional Map</a>

<br>
							<p class="text-center"><small>Advertisement</small></p><div id="acrow-ad" class="acrow-ad">
							<a href="https://www.acrow.com" target="_blank" class="acrow-ad"><img src="/img/Acrow_350x350.jpg" style="width: 100%; padding: 0 20%;" class="acrow-ad"></a>
							</div>
@endsection

@section('js')
	<script type="text/javascript">
	setTimeout(function(){ $("#map-pr").show(); }, 500);
	if($( window ).width() > 700){
		$("#right-aside").sticky({topSpacing:0});
	}
		$("#statequickselect").on("change", function(){
			var state = $(this).val();
			window.location.href = "/state/profile/" + state;
		});
	</script>
@endsection