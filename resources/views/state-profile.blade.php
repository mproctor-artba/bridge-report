@extends('layouts.dual-aside')

@section('css')
<style type="text/css">
	.table-striped thead tr th{
		background: #004064;
		color:white;
		font-size: 14px;
	}
	.table-striped tr.active td{
		background: #004064 !important;
		color:white;
	}
	.table-striped tbody tr td{
		font-size: 13px;
	}
	.table-striped.light tr th{
		background: rgb(214,220,229);
		color:inherit;
	}
	canvas {
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		height:310px;
	}
	th, thead {
  position: sticky;
}
.accordion-toggle{
	display: block;
	width:100%;
}
.wrap table{
	table-layout: fixed;
	margin-bottom: 0;
}
.inner-table{

	max-height: 450px; 
	overflow-y: scroll;
}
.panel-group .panel{
	margin-bottom: 20px;
}
.button-list{
	list-style: none; 
	padding:0; 
	margin-left: -32px;
}
.button-list li{
	margin-bottom: 15px;
}
.button-list li a{
	width:100%;
	font-size: 13px;
}
.navbar-brand-pdf{
	display: none;
}
.navbar-brand-pdf{
	display: none;
}
</style>
@endsection

@section('aside')

@endsection

@section('content')

	<div class="row">
		<div class='col-md-8 text-left' style="">
				<a class="navbar-brand-pdf" href="https://www.artbabridgereport.org"><img src="https://www.artba.org/wp-content/uploads/2023/08/ARTBA-logo@2x.png" style="width: 160px"></a>
		    <h1 class='page-title'>{{ $text->title }}</h1>
		    <hr></hr>
		</div>
		<div class="col-md-4 text-right">
		<br>
			<div id="districtResults" class="pdfcrowd-remove"></div>
		</div>
	</div>
	<div class="row mb-10">
		<div class="col-md-9 desktop-padding">
			<ul>
				<li>{{ $text->bullet1 }}</li>
				<li>{{ $text->bullet2 }}</li>
				<li>{{ $text->bullet3 }}</li>
				<li>{{ $text->bullet4 }}</li>
				<li>{{ $text->bullet5 }}</li>
				<li>{{ $text->bullet6 }}</li>
				<li>{{ $text->bullet7 }}</li>
			</ul>
			<br>			
		</div>
		<div class="col-md-3 pdfcrowd-remove">
			<ul class="button-list">
				<li><a type="button" id="exportPDF" href="/economic-reports/export-pdf?state={{ $rankings->state_abbr}}" class="btn" target="_blank" style="background: #16a085; color:white !important; ">Download {{ $text->stname }} Report (PDF)</a></li>
				<li><a href="mailto:jschneidawind@artba.org" class="btn" style="background: #2e81ab; color:white !important;" target="_blank">Media Inquiries</a></li>
			</ul>
		</div>
	</div>
	<div id="right-aside-exported"></div>
	<div class="row mb-10">
		<div class="dynamic-cards" style="display: flex; flex-direction: row; justify-content: space-between;">
			<div class="col-md-6" style="min-height: 300px; max-width: 500px;">
				<div class="chart-container" style="min-height: 30px;">
					<canvas id="canvas" style="min-height: 300px;"> </canvas>
				</div>
			</div>
			<div class="col-md-6" style="min-height: 300px; max-width: 500px;">
				<div class="chart-container" style="min-height: 300px;">
					<canvas id="canvastwo" style="min-height: 300px;"></canvas>
				</div>
			</div>
		</div>
	</div>
	<div style="page-break-before:always"></div>
	<div class="row" style="margin-top:20px;">
		<div class="col-md-12">
			<h3 class="">Top Most Traveled Structurally Deficient Bridges in {{ $text->stname }}</h3>
			<table class="table table-striped">
				<thead>
					<tr>
						<th class="text-left">County</th>
						<th class="text-right">Year Built</th>
						<th class="text-right">Daily Crossings</th>
						<th class="text-left">Type of Bridge</th>
						<th class="text-left">Location</th>
					</tr>
				</thead>
				<tbody>
						@foreach($topten as $top)
							<tr>
								<td class="text-left">{{ $top->county }}</td>
								<td class="text-right">{{ $top->built }}</td>
								<td class="text-right">{{ number_format($top->crossings) }}</td>
								<td class="text-left">{{ $top->type }}</td>
								<td class="text-left">{{ $top->location }}</td>
							</tr>
						@endforeach
					</tbody>
			</table>
		</div>
	</div>
	<div style="page-break-before:always"></div>
	<div class="row" style="margin-top:20px;">
		<div class="col-md-12">
			<h3 class="">Bridge Inventory: {{ $text->stname }}</h3>
			<table class="table table-striped">
				<thead>
					<tr class="row-1 odd" role="row">
						<th class="text-left"  style="">Type of Bridge</th>
						<th class="text-right" style="">Number of Bridges</th>
						<th class="text-right" style="">Area of All Bridges <br>(sq. meters)</th>
						<th class="text-right" style="">Daily Crossings on All Bridges</th>
						<th class="text-right" style="">Number of Structurally Deficient Bridges</th>
						<th class="text-right" style="">Area of Structurally Deficient Bridges <br>(sq. meters)</th>
						<th class="text-right" style="">Daily Crossings on Structurally Deficient Bridges</th>
						</tr>
				</thead>
				<tbody>
					@foreach($inventory as $bridge)
							<tr>
								<td class="text-left">{{ $bridge->type }}</td>
								<td class="text-right">{{ number_format($bridge->quantity) }}</td>
								<td class="text-right">{{ number_format($bridge->area) }}</td>
								<td class="text-right">{{ number_format($bridge->crossings) }}</td>
								<td class="text-right">{{ number_format($bridge->num_deficient) }}</td>
								<td class="text-right">{{ number_format($bridge->area_deficient) }}</td>
								<td class="text-right">{{ number_format($bridge->crossings_deficient) }}</td>
							</tr>
						@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="row" style="margin-top:20px;">
		<div class="col-md-12">
				<h3 class="">Proposed Bridge Work</h3>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="text-left" style="">Type of Work</th>
							<th class="text-right" style="">Number of Bridges</th>
							<th class="text-right" style="">Cost to Repair <br>(in millions)</th>
							<th class="text-right" style="">Daily Crossings</th>
							<th class="text-right" style="">Area of Bridges <br>(sq. meters)</th>
						</tr>
					</thead>
					<tbody>
						@foreach($proposals as $proposal)
							<tr>
								<td class="text-left">{{ $proposal->type }}</td>
								<td class="text-right">{{ number_format($proposal->number) }}</td>
								<td class="text-right">${{ number_format($proposal->cost) }}</td>
								<td class="text-right">{{ number_format($proposal->crossings) }}</td>
								<td class="text-right">{{ number_format($proposal->area) }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
		</div>
	</div>
	<div style="page-break-before:always"></div>
	<div class="row">
		<div class="col-md-12 text-small">
			<h4>About the data:</h4>
			<p>Data and cost estimates are from the Federal Highway Administration (FHWA) National Bridge Inventory (NBI), downloaded on August 20, 2024. Note that specific conditions on bridges may have changed as a result of recent work or updated inspections.</p>
			<p>
			Effective January 1, 2018, FHWA changed the definition of structurally deficient as part of the final rule on highway and bridge performance measures, published May 20, 2017 pursuant to the 2012 federal aid highway bill Moving Ahead for Progress in the 21st Century Act (MAP-21).  Two measures that were previously used to classify bridges as structurally deficient are no longer used.  This includes bridges where the overall structural evaluation was rated in poor or worse condition, or where the adequacy of waterway openings was insufficient.  </p>
			<p>
			The new definition limits the classification to bridges where one of the key structural elements—the deck, superstructure, substructure or culverts, are rated in poor or worse condition.  During inspection, the conditions of a variety of bridge elements are rated on a scale of 0 (failed condition) to 9 (excellent condition).  A rating of 4 is considered “poor” condition. </p>
			<p>
			Cost estimates have been derived by ARTBA, based on 2023 average bridge replacement costs for structures on and off the National Highway System, <a href="https://www.fhwa.dot.gov/bridge/nbi/sd.cfm" target="_blank">published by FHWA</a>. Bridge rehabilitation costs are estimated to be 68 percent of replacement costs.  A bridge is considered to need repair if the structure has identified repairs as part of the NBI, a repair cost estimate is supplied by the bridge owner or the bridge is classified as structurally deficient.  Please note that for a few states, the number of bridges needing to be repaired can vary significantly from year to year, and reflects the data entered by the state.</p>
			<p>
			Bridges are classified by FHWA into types based on the functional classification of the roadway on the bridge. Interstates comprise routes officially designated by the Secretary of Transportation. Other principal arterials serve major centers of urban areas or provide mobility through rural areas. Freeways and expressways have directional lanes generally separated by a physical barrier, and access/egress points generally limited to on- and off-ramps. Minor arterials serve smaller areas and are used for trips of moderate length. Collectors funnel traffic from local roads to the arterial network; major collectors have higher speed limits and traffic volumes and are longer in length and spaced at greater intervals, while minor collectors are shorter and provide service to smaller communities. Local roads do not carry through traffic and are intended for short distance travel.
			</p>
		</div>
	</div>
@endsection

@section('right-side')
<div class="dynamic-cards">
	@include('layouts.percent-rank-card')
	@include('layouts.num-rank-card')
	@include('layouts.area-rank-card')
	@include('layouts.news-card')
</div>
	<a href="{{ route('state-ranking') }}" class="btn btn-danger full-width pdfcrowd-remove">Full State Ranking</a>
	@include('layouts.acrow-ad')
@endsection

@section('js')
	<script type="text/javascript" src="https://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
	<script src="/js/utils.js"></script>
	<script src="https://unpkg.com/floatthead"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
	<script>
		var color = Chart.helpers.color;

		@include('layouts.chart-top')

		var colorNames = Object.keys(window.chartColors);

		@include('layouts.chart-bottom')

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			ctx.height = 500;
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					tooltips: {
    					callbacks: {
    						label: function(value){
    							return "Bridges: " + numberWithCommas(value.yLabel)}
    					}
    				},
					responsive: true,
					maintainAspectRatio: false,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Number of Bridges in Need of Repair, Including Structurally Deficient Bridges'
					}
					,
					scales: {
			      		yAxes: [{
			        		ticks: {
			        			stepSize: {{ setStepper($chartonevalues)["step"] }},
			        			min: {{ setStepper($chartonevalues)["min"] }},
			        			max: {{ setStepper($chartonevalues)["max"] }},
			        			callback: function(value){return numberWithCommas(value)}
			        		}
			        	}]
			        }
			        
				}
			});

			var ctx = document.getElementById('canvastwo').getContext('2d');
			ctx.height = 500;
			window.myMixedChart = new Chart(ctx, {
				type: 'bar',
				data: chartData,
				height:500,
				options: {
					tooltips: {
						mode: "index",
    					callbacks: {
    							label: function(t, d) {
    							if(t.datasetIndex == 1){
    								// bar chart
    								return "Structurally Deficient Bridges: " + numberWithCommas(t.yLabel);
    							}
    							if(t.datasetIndex == 0){
    								return "% of All Bridges Classified as Structurally Deficient: " + t.yLabel + "%";
    							}
    						}
    					}
    				},
					responsive: true,
					maintainAspectRatio: false,
					height:500,
					title: {
						display: true,
						text: 'Number of Structurally Deficient Bridges'
					},
					showTooltips: true,
					scales: {
			      		yAxes: [{
			        		id: 'A',
			        		type: 'linear',
        					stacked: false,
			        		position: 'right',
			        		ticks: {
			        			beginAtZero: true,
                   				callback: function(value){return value+ "%"}
			        		},scaleLabel: {
			                   display: true,
			                   labelString: "Percentage"
			                }
			      		},
			      		{
			        		id: 'B',
			        		type: 'linear',
        					stacked: false,
			        		position: 'left',
			        		ticks: {
			        			stepSize: {{ setStepper($charttwovalues)["step"] }},
			        			min: {{ setStepper($charttwovalues)["min"] }},
			        			max: {{ setStepper($charttwovalues)["max"] }},
			        			callback: function(value){return numberWithCommas(value)}
			        		}
			      		}]
			    	}
				}
			});
		};
			@if($rankings->state_abbr != "PR")
			$.get("/congressional/reps/bystate/{{ $rankings->state_abbr }}" , function( data ) {
			  	$("#districtResults").html(data);
			});
			@endif
			$("#districtResults").on("change", "#members", function(){

			
			var district = $(this).children(":selected").attr("data-district");
			var state = $("#district-"+district).attr("data-state");
			window.location.href = "/congressional/district/" + state + "/" + district;

			if(district > 0){
				window.location.href = "/congressional/district/" + state + "/" + district;
			}
			else {
				window.location.href = "/state/profile/" + state;
			}
		});

			$(".carousel").swipe({

  swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

    if (direction == 'left') $(this).carousel('next');
    if (direction == 'right') $(this).carousel('prev');

  },
  allowPageScroll:"vertical"

});

	function numberWithCommas(x) {
    	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	</script>
@endsection