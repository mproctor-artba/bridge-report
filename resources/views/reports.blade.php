@extends('layouts.dual-aside')

@section('css')
<style type="text/css">
    img.doc-icon{
        height: 150px;
        margin: 0 auto;
    }
    .doc-title{
        min-height: 62px;
    }
    .doc-title strong{
        font-size: 15px;
    }
</style>
@endsection

@section('content')
    
<div class='col-md-12 text-left'>
    <h1 class='page-title'>Reports</h1>
    <div class="row mb20">
        <div class="col-md-12">
            <div class="col-md-4 text-center">
                <a href="/reports/2020 ARTBA Bridge Report.pdf" target="_blank"><img src="/img/pdf-report.png" class="doc-icon"></a>
                <br>
                <div class="doc-title">
                <strong>2020 Bridge Report</strong>
                </div>
                <a href="/reports/2020 ARTBA Bridge Report.pdf" target="_blank">View Full Report</a>
            </div>
            <div class="col-md-4 text-center">
                <a href="/reports/ARTBA 2020 Bridge Report - State Ranking.pdf" target="_blank"><img src="/img/pdf-report.png" class="doc-icon"></a>
                <br>
                <div class="doc-title">
                <strong>State Rankings</strong>
                </div>
                <a href="/reports/ARTBA 2020 Bridge Report - State Ranking.pdf" target="_blank">View Full Report</a>
            </div>
            
            <div class="col-md-4 text-center">
                <a href="/reports/ARTBA 2020 Bridge Report - Top 250 Bridges.pdf" target="_blank"><img src="/img/pdf-report.png" class="doc-icon"></a>
                <br>
                <div class="doc-title">
                <strong>Most traveled U.S. Structurally Deficient Bridges</strong>
                </div>
                <a href="/reports/ARTBA 2020 Bridge Report - Top 250 Bridges.pdf" target="_blank">View Full Report</a>
            </div>
        </div>
    </div>
    <div class="row mb20">
        <div class="col-md-12">
            <div class="col-md-4 text-center">
                <a href="/reports/ARTBA 2020 Bridge Report - Top 10 Bridges by State.pdf" target="_blank"><img src="/img/pdf-report.png" class="doc-icon"></a>
                <br>
                <div class="doc-title">
                <strong>Top 10 Most Traveled U.S. Structurally Deficient Bridges by State</strong>
            </div>
                <br>
                <a href="/reports/ARTBA 2020 Bridge Report - Top 10 Bridges by State.pdf" target="_blank">View Full Report</a>
            </div>
            <div class="col-md-4 text-center">
                <a href="/reports/ARTBA_Federal_Funding_Map Ten-Year 11-26-19.pdf" target="_blank"><img src="/img/pdf-report.png" class="doc-icon"></a>
                <br>
                <div class="doc-title">
                <strong>Federal funds, on average, provide 51% of annual State DOT capital outlays for highway & bridge projects</strong>
            </div>
                <br>
                <a href="/reports/ARTBA_Federal_Funding_Map Ten-Year 11-26-19.pdf" target="_blank">View Full Report</a>
            </div>
            <div class="col-md-4 text-center">
                <a href="https://store.artba.org/product/2021-u-s-transportation-construction-market-forecast-report-with-webinar-replay/" target="_blank"><img src="https://store.artba.org/wp-content/uploads/2020/12/2021-Forecase-Cover.png" class="doc-icon"></a>
                <br>
                <div class="doc-title">
                <strong>2021 U.S. Transportation Construction Market Forecast Report for highway, bridge, airport runway, transit, freight, rail, and ports/waterway markets.
                </strong>
            </div>
                <br>
                <a href="https://store.artba.org/product/2021-u-s-transportation-construction-market-forecast-report-with-webinar-replay/" target="_blank">Purchase Full Report</a>
            </div>
        </div>
    </div>
    <div class="row mb20">
        <div class="col-md-12">
            <h3>Film Placeholder</h3>
            <iframe width="100%" height="400" src="https://www.youtube.com/embed/QLlo1qrAH7Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <br>
            <a href="{{ route('memorial-bridge') }}" class="btn btn-danger">Learn More -></a>
        </div>
    </div>
</div>

@endsection

@section('right-side')
    <h3 class="text-center" style="margin-bottom: 0;">Member Benefits</h3>

    <p>ARTBA’s communications platform and its exclusive focus on transportation keeps its members up-to-speed and provides analysis in real time on policy, regulatory, legal and safety developments from Washington, D.C. ARTBA’s economic research and analyses also provide members with the market intelligence they need to make smart business decisions</p>
    
    <ul>
        <li>Bridge Inventory</li>
        <li>Discounted Industry Forecasts</li>
    </ul>
    
    <a href="https://www.artba.org/artbamembers/" class="btn btn-danger btn-flat btn-full-width">Become a Member</a>
@endsection

@section('js')
    <script>
    $(document).ready(function(){
        if($( window ).width() > 700){
        $("#right-aside").sticky({topSpacing:0});
    }
      });
    </script>
@endsection