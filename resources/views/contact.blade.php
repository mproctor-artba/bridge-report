@extends('layouts.right-aside')

@section('content')
    
<div class='col-md-12 text-left'>
    <h1 class='page-title'>Bridge News</h1>
    
</div>

@endsection

@section('right-side')
	<h3 class="text-center" style="margin-bottom: 0;">Member Benefits</h3>

	<p>ARTBA’s communications platform and its exclusive focus on transportation keeps its members up-to-speed and provides analysis in real time on policy, regulatory, legal and safety developments from Washington, D.C. ARTBA’s economic research and analyses also provide members with the market intelligence they need to make smart business decisions</p>
	
	<ul>
		<li>Bridge Inventory</li>
		<li>Discounted Industry Forecasts</li>
	</ul>
	
	<a href="https://www.artba.org/artbamembers/" class="btn btn-danger btn-flat btn-full-width">Become a Member</a>
@endsection

@section('js')
	<script>
	$(document).ready(function(){
	    $("#right-aside").sticky({topSpacing:0});
	  });
	</script>
@endsection