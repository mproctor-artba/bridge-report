@extends('layouts.right-aside')

@section('css')
	<style type="text/css">
		#table-head-sticky-wrapper{
			display: contents;
		}
	</style>
@endsection

@section('content')

<div class='col-md-12 text-center'>
    <h1 class='page-title'>Most Traveled U.S. Structurally Deficient Bridges</h1>
    <p class="header-caption"></p>
</div>
<div class='col-md-12 text-center'>
	 <table id="datatable-buttons" class="display table-condensed table table-hover table-bordered dt-responsive responsive-display">
        <thead id="table-head">
        <tr style="background-color: #005583">
			<th id="th-buttons" class="dt-buttons table-header-buttons" colspan="7" rowspan="1"></th>
		</tr>
            <tr>
                <th class="text-center"><span id="currentYear">{{ env('CURRENT_YEAR') }}</span> Rank</th>
                <th class="text-left">State</th>
                <th class="text-left">County</th>
                <th class="text-center">Year Built</th>
                <th class="text-center">Avg. Daily Travel</th>
                <th class="text-left">Type</th>
                <th class="text-left">Location</th>
            </tr>
        </thead>
        <tbody id="state-ranking"></tbody>
    </table>
</div>
<div class="col-md-12 text-center">

</div>

@endsection

@section('right-side')
	<br>
	<br>
	@include('layouts.acrow-ad')
	<!--
	<a href="/reports/ARTBA 2020 Bridge Report - Top 250 Bridges.pdf" target="_blank" class="btn btn-danger btn-flat btn-full-width text-white" style="margin-top: 10px;">Download Full PDF</a>

-->
@endsection

@section('js')
	<script src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.1/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
    	$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': "{{ csrf_token() }}"
	          }
	        });
    	var paging = true;

    	if($( window ).width() > 700){
    		paging = false;
    	}

	    	var table = $('#datatable-buttons').DataTable({
			responsive: true,
			paging: paging,
	  		ajax: {
		    	"url":"/state/ranking/top-bridges",
		    	"type": "POST",
		    	"data": function (d){
		     		d.year = {{ env('CURRENT_YEAR') }}
		    }
		  },
			columns: [
		    { "data": "rank"},
		    { "data": "state", "class" : "text-left"},
		    { "data": "county", "class" : "text-left"},
		    { "data": "built"},
		    { "data": "adt"},
		    { "data": "type", "class" : "text-left"},
		    { "data": "location", "class" : "text-left"}
		  ],
		});

		new $.fn.dataTable.Buttons(table, {
			// data-spy="afix" data-offset-top="#navbar"
			buttons: [
				'pdf', 'excel','csv'
			],

		});

		

	    $("body").on("click", "#select2-result-label-5", function(){
	    	var years = "";
	    	$(this + "ul li").each(function(){
	    		var year = $(this).text();
	    		years += ", " + year;
	    	});
	    	console.log(years);
	    	var year = $(this).val();
	    	var table = $('#datatable-buttons').DataTable();
	    	$("#currentYear").text(year);

	    	table.clear();
	    	table.ajax.reload();
	    });
	});
	</script>
@endsection