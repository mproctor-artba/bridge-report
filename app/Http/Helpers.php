<?php

use App\Models\DistrictText as DistrictText;
use App\Models\StateRanking as StateRankings;

function setStepper($data){
	$data = $data->toArray();

	if(array_key_exists('rep_count', $data[0])){



		usort($data, function ($item1, $item2) {
	    	return ($item2['rep_count'] <=> $item1['rep_count']);
		});
		$max = roundNearestHundredUp($data[0]["rep_count"] * 1.05);

		$min = $data[4]["rep_count"];
		$adjust = $min/$max;
		$min = roundNearestHundredUp($min * $adjust * 0.87);
		if($min <= 100){
			$min = 0;
		}
		if($max <= 400){
			$min = 0;
		}



		if($data[4]["rep_count"] <= 30){
			$max = 30;
		}

	}
	else{
		usort($data, function ($item1, $item2) {
	    	return ($item2['def'] <=> $item1['def']);
		});
		$max = roundNearestHundredUp($data[0]["def"] * 1.05);

		$min = $data[4]["def"];

		if($min == 0 && $max == 0){
		return ["step" => 10, "min" => 0, "max" => 50];
	}

		$adjust = $min/$max;
		$min = roundNearestHundredUp($min * $adjust * 0.82);
		
		if($min < 120){
			$min = 0;
		}
		if($max <= 100){
			$max = 80;
		}
		if($max <= 200 && $max > 100){
			$max = 200;
		}
		if($data[0]["def"] <= 40){
			$max = 40;
		}
		if($data[0]["def"] <= 10){
			$max = 10;
		}
		if($data[0]["def"] <= 5){
			$max = 5;
		}
		
	}

	if($min == $max){
		$min = 0;
	}
	if($max < $min){
		$max = $min * 1.5;
		if($max <= 300){
			$min = 0;
		}
		
	}

	if($min == 0 && $max == 0){
		return ["step" => 10, "min" => 0, "max" => 50];
	}

	
	$step = stepRange(4, $min, $max);
	if(!isset($step[1])){
		
	}
	$step = roundNEarestTenUp($step[1] - $step[0]);
	return ["step" => $step, "min" => $min, "max" => $max];

	
}

function roundNearestHundredUp($number){
	return ceil( $number / 100 ) * 100;
}

function roundNearestTenUp($number){
	return ceil($number / 10) * 10;
}

function stepRange($wanted, $rangeLow, $rangeHigh){
    $increment=($rangeHigh-$rangeLow)/($wanted+1);
    $r=array();
    for ($i=$rangeLow+$increment;$i<$rangeHigh;$i+=$increment)
        $r[]=$i;
    return $r;
}

function mapColor($color){
	switch ($color) {
		case "L":
			return [0 => env('MAP_L1'), 1 => env('MAP_L2')];
			break;
		case "M":
			return [0 => env('MAP_M1'), 1 => env('MAP_M2')];
			break;
		case "H":
			return [0 => env('MAP_H1'), 1 => env('MAP_H2')];
			break;
		default:
			return [0 => env('MAP_L1'), 1 => env('MAP_L2')];
			break;
	}
}

function footnote($id){
	$text = DistrictText::find($id);
	return $text->footnote . $text->footnote2 . $text->footnote3 . $text->footnote4 . $text->footnote5;
}

function NumRankOld($state){
	return StateRankings::where('state', $state)->where('year', env('PAST_YEAR'))->first()->num_rank;
}

function PercentRankOld($state){
	return StateRankings::where('state', $state)->where('year', env('PAST_YEAR'))->first()->percent_rank;
}

function AreaRankOld($state){
	return StateRankings::where('state', $state)->where('year', env('PAST_YEAR'))->first()->area_rank;
}
