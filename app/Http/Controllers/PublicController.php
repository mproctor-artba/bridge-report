<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StateRanking as StateRankings;
use App\Models\StateInventory as StateInventory;
use App\Models\StateProposal as StateProposals;
use App\Models\StateTopTen as StateTopTen;
use App\Models\StateText as StateText;
use App\Models\StateMap as StateMaps;
use App\Models\DistrictText as DistrictText;
use App\Models\DistrictTopBridge as DistrictTopBridges;
use App\Models\DistrictInventory as DistrictInventory;
use App\Models\DistrictProposal as DistrictProposals;
use App\Models\DistrictMap as DistrictMaps;
use App\Models\Zipcode as Zipcodes;
use App\Models\TopBridge as TopBridges;
use App\Models\ChartOneState as ChartOneState;
use App\Models\ChartTwoState as ChartTwoState;
use App\Models\ChartOneDistrict as ChartOneDistrict;
use App\Models\ChartTwoDistrict as ChartTwoDistrict;
use DB;
use Redirect;
use SoapBox\Formatter\Formatter;

use Pdfcrowd\HtmlToPdfClient as PDFCrowd;

class PublicController extends Controller
{
    public function index(){

        $districts = DistrictText::where('year', env('CURRENT_YEAR'))->get();
/*
        foreach ($districts as $district) {
            $url = 'http://bridges.artba.org/congressional/district/' . $district->state_abbr . '/' . $district->cd;
           // echo " url: " . "<a href='" . $url . "'>" . $url . "</a>";
            //echo "<br>" . $district->stname . " " . $district->cd;

            // create curl resource 
            
            $ch = curl_init(); 

            // set url 
            curl_setopt($ch, CURLOPT_URL, $url); 

            //return the transfer as a string 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

            // $output contains the output string 
            $output = curl_exec($ch); 

            // close curl resource to free up system resources 
            curl_close($ch);  
            echo "<br><a href='http://localhost:8000/congressional/district/" . $district->state_abbr . '/' . $district->cd. "'>$url</a>: " . strlen($output). "<br>";

            sleep(0.5);
            
        }

        */
        
        $data = [
            "page" => "index",
            "states" => DB::table('state_rankings')->select('state','state_abbr','percent_rank')->where('percent_rank','>',0)->where('year',env('CURRENT_YEAR'))->distinct()->limit('25')->orderBy('percent_rank','ASC')->get()
        ];



        return view('welcome', $data);
        
    }
    public function about(){
        $data = [
            "title" => "About | ARTBA Bridge Report",
            "page" => "about"
        ];

        return view('about', $data);
    }

    public function reports(){
        $data = [
            "title" => "reports | ARTBA Bridge Report",
            "page" => "reports"
        ];

        return view('reports', $data);
    }

    public function memorialBridge(){
        $data = [
            "page" => "reports"
        ];

        return view('memorial', $data); //Redirect::to('/'); //
    }

    public function news(){
        $url = 'https://newsline.artba.org/category/bridge-report/feed/?i=' .rand(100,999);
        $xml = Formatter::make(file_get_contents($url), Formatter::XML);
        $stories = $xml->toArray()["channel"];

        $data = [
            "page" => "news",
            "stories" => $stories
        ];

        return view('news', $data);
    }

    public function contact(){
        $data = [
            "page" => "contact"
        ];

        return view('contact', $data);
    }

    public function stateMap(){
        $states = StateMaps::where('year', env('CURRENT_YEAR'))->orderBy('state', 'ASC')->get();

        $map = [
            "mapWidth" => "100%",
            "mapHeight" => 310,
            "shadowWidth" => 2,
            "shadowOpacity" => 0.2,
            "shadowColor" => "black",
            "shadowX" => 0,
            "shadowY" => 0,
            "iPhoneLink" => true,
            "isNewWindow" => false,
            "zoomEnable" => false,
            "zoomEnableControls" => false,
            "zoomIgnoreMouseScroll" => false,
            "zoomMax" => 8,
            "zoomStep" => 0.8,
            "borderColor" => "#ffffff",
            "borderColorOver" => "#ffffff",
            "borderOpacity" => 0.5,
            "nameColor" => "#ffffff",
            "nameColorOver" => "#ffffff",
            "nameFontSize" => "10px",
            "nameFontWeight" => "bold",
            "nameStroke" => false,
            "nameStrokeColor" => "#000000",
            "nameStrokeColorOver" => "#000000",
            "nameStrokeWidth" => 1.5,
            "nameStrokeOpacity" => 0.5,
            "overDelay" => 300,
            "nameAutoSize" => false,
            "tooltipOnHighlightIn" => false,
            "freezeTooltipOnClick" => false,
            "map_data" => []
        ];
        
        foreach ($states as $state) {
            $map["map_data"]["st".$state->id] = [
                "id" => "$state->id",
                "name" => "$state->state",
                "shortname" => "$state->state_abbr",
                "link" => "/state/profile/$state->state_abbr",
                "comment" => "",
                "image" => "",
                "color_map" => env("MAP_" . $state->color . "1"),
                "color_map_over" => env("MAP_" . $state->color . "2"),
                "isNewWindow" => 1
            ];
        }

        $data = [
            "title" => "State Map | ARTBA Bridge Report",
            "page" => "state",
            "map" => json_encode($map)
        ];

        return view('state-map', $data);
    }
    public function stateRanking(){

        $data = [
            "title" => "State Rankings | ARTBA Bridge Report",
            "rankings" => StateRankings::where('num_rank','>', 0)->where('year',env('CURRENT_YEAR'))->orderBy('num_rank', 'ASC')->get(),
            "page" => "state-ranking"
        ];

        return view('state-ranking', $data);
    }

    public function topBridges(){
        $data = [
            "title" => "Top Bridges | ARTBA Bridge Report",
            "bridges" => TopBridges::where('year',env('CURRENT_YEAR'))->get(),
            "page" => "top-bridges"
        ];

        return view('top-bridges', $data);
    }

    public function stateProfile($state){
        $stateRanking = StateRankings::where('year', env('CURRENT_YEAR'))->where('state_abbr', $state)->first();

        if(!isset($stateRanking)){
            abort(404);
        }
        
        $data = [
            "title" => "$state | ARTBA Bridge Report",
            "rankings" => $stateRanking,
            "inventory" => StateInventory::where('year', env('CURRENT_YEAR'))->where('state', $state)->get(),
            "proposals" => StateProposals::where('year', env('CURRENT_YEAR'))->where('state', $state)->get(),
            "topten" => StateTopTen::where('year', env('CURRENT_YEAR'))->where('state', $stateRanking->state_abbr)->get(),
            "text" => StateText::where('year', env('CURRENT_YEAR'))->where('state_abbr', $state)->first(),
            "defnum_neighborUp" => StateRankings::where('year', env('CURRENT_YEAR'))->where('num_rank', $stateRanking->num_rank - 1)->first(),
            "defnum_neighborDown" => StateRankings::where('year', env('CURRENT_YEAR'))->where('num_rank', ($stateRanking->num_rank + 1))->first(),
            "defpercent_neighborUp" => StateRankings::where('year', env('CURRENT_YEAR'))->where('percent_rank', $stateRanking->percent_rank - 1)->first(),
            "defpercent_neighborDown" => StateRankings::where('year', env('CURRENT_YEAR'))->where('percent_rank', $stateRanking->percent_rank + 1)->first(),
            "defarea_neighborUp" => StateRankings::where('year', env('CURRENT_YEAR'))->where('area_rank', $stateRanking->area_rank - 1)->first(),
            "defarea_neighborDown" => StateRankings::where('year', env('CURRENT_YEAR'))->where('area_rank', ($stateRanking->area_rank + 1))->first(),
            "best_numrank" => StateRankings::where('year', env('CURRENT_YEAR'))->where('num_rank', 1)->first(),
            "best_percent_rank" => StateRankings::where('year', env('CURRENT_YEAR'))->where('percent_rank', 1)->first(),
            "best_area_rank" => StateRankings::where('year', env('CURRENT_YEAR'))->where('area_rank', 1)->first(),
            "chartonevalues" => ChartOneState::where('state', $state)->where('year', '>=', env('PAST_YEAR') - 3)->orderBy('year', 'ASC')->get(),
            "charttwovalues" => ChartTwoState::where('state', $state)->where('year', '>=', env('PAST_YEAR') - 3)->orderBy('year', 'ASC')->get(),
            "page" => "state"
        ];

        return view('state-profile', $data);
    }

    public function districtProfile($state, $district){
        $stateRanking = StateRankings::where('year', env('CURRENT_YEAR'))->where('state_abbr', $state)->first();
        $cdText = DistrictText::where('year', env('CURRENT_YEAR'))->where('state_abbr', $state)->where('cd', $district)->first();
        if(!isset($stateRanking) || !isset($cdText)){
            abort(404);
        }

        $data = [
            "title" => "$state District $district | ARTBA Bridge Report",
            "rankings" => $stateRanking,
            "inventory" => DistrictInventory::where('year', env('CURRENT_YEAR'))->where('state', $state)->where('cd', $district)->get(),
            "proposals" => DistrictProposals::where('year', env('CURRENT_YEAR'))->where('state', $state)->where('cd', $district)->get(),
            "text" => $cdText,
            "topten" => DistrictTopBridges::where('year', env('CURRENT_YEAR'))->where('state', $state)->where('cd', $district)->get(),
            "defnum_neighborUp" => StateRankings::where('year', env('CURRENT_YEAR'))->where('num_rank', $stateRanking->num_rank - 1)->first(),
            "defnum_neighborDown" => StateRankings::where('year', env('CURRENT_YEAR'))->where('num_rank', ($stateRanking->num_rank + 1))->first(),
            "defpercent_neighborUp" => StateRankings::where('year', env('CURRENT_YEAR'))->where('percent_rank', $stateRanking->percent_rank - 1)->first(),
            "defpercent_neighborDown" => StateRankings::where('year', env('CURRENT_YEAR'))->where('percent_rank', $stateRanking->percent_rank + 1)->first(),
            "defarea_neighborUp" => StateRankings::where('year', env('CURRENT_YEAR'))->where('area_rank', $stateRanking->area_rank - 1)->first(),
            "defarea_neighborDown" => StateRankings::where('year', env('CURRENT_YEAR'))->where('area_rank', ($stateRanking->area_rank + 1))->first(),
            "best_numrank" => StateRankings::where('year', env('CURRENT_YEAR'))->where('num_rank', 1)->first(),
            "best_percent_rank" => StateRankings::where('year', env('CURRENT_YEAR'))->where('percent_rank', 1)->first(),
            "best_area_rank" => StateRankings::where('year', env('CURRENT_YEAR'))->where('area_rank', 1)->first(),
            "chartonevalues" => ChartOneDistrict::where('state', $state)->where('cd', $district)->where('year', '>=', env('PAST_YEAR') - 3)->orderBy('year', 'ASC')->get(),
            "charttwovalues" => ChartTwoDistrict::where('state', $state)->where('cd', $district)->where('year', '>=', env('PAST_YEAR') - 3)->orderBy('year', 'ASC')->get(),
            "page" => "district",

            "district" => $district
        ];

        return view('congressional-profile', $data);
    }

    public function congressionalMap(){
        $states = DistrictMaps::where('year', env('CURRENT_YEAR'))->orderBy('state', 'ASC')->get();

        $map = [
            "mapWidth" => "100%",
            "mapHeight" => 310,
            "shadowWidth" => 2,
            "shadowOpacity" => 0.2,
            "shadowColor" => "black",
            "shadowX" => 0,
            "shadowY" => 0,
            "iPhoneLink" => true,
            "isNewWindow" => false,
            "zoomEnable" => false,
            "zoomEnableControls" => false,
            "zoomIgnoreMouseScroll" => false,
            "zoomMax" => 8,
            "zoomStep" => 0.8,
            "borderColor" => "#ffffff",
            "borderColorOver" => "#ffffff",
            "borderOpacity" => 0.5,
            "nameColor" => "#ffffff",
            "nameColorOver" => "#ffffff",
            "nameFontSize" => "10px",
            "nameFontWeight" => "bold",
            "nameStroke" => false,
            "nameStrokeColor" => "#000000",
            "nameStrokeColorOver" => "#000000",
            "nameStrokeWidth" => 1.5,
            "nameStrokeOpacity" => 0.5,
            "overDelay" => 300,
            "nameAutoSize" => false,
            "tooltipOnHighlightIn" => false,
            "freezeTooltipOnClick" => false,
            "map_data" => []
        ];

        foreach ($states as $state) {
            if($state->district > 0){
                $link = "/congressional/district/$state->state_abbr/$state->district";
            } else {
                $link = "/state/profile/$state->state_abbr";
            }
            $map["map_data"]["st".$state->id] = [
                "id" => "$state->id",
                "name" => "$state->state Congressional District $state->district",
                "link" => $link,
                "comment" => "",
                "image" => "",
                "color_map" => env("MAP_" . $state->color . "1"),
                "color_map_over" => env("MAP_" . $state->color . "2"),
                "isNewWindow" => 1
            ];
        }

        $data = [
            "title" => "Congressional Map | ARTBA Bridge Report",
            "page" => "district",
            "map" => json_encode($map)
        ];

        return view('congressional-map', $data);
    }

    public function queryDistricts($method, $query){
        $html = "<select id='members' class='form-control'><option val=''>Select Congressional District</option>";

        $members = NULL;

        if($method == "bystate"){
            $members = DB::table('zipcodes')->select('full_name', 'cd','state_abb')->distinct()->where('state_abb', $query)->orderBy('cd','ASC')->get();
        }
        elseif($method == "byzipcode"){
            $members = DB::table('zipcodes')->select('full_name', 'cd', 'state_abb')->distinct()->where('zip', $query)->get();
        }
        foreach($members as $member){
            $html .= "<option id='district-" . $member->cd . "' data-state='" . $member->state_abb . "' data-district='" . $member->cd . "'>District " . $member->cd . "</option>";
        }

        $html .= "</select>";

        return $html;
    }

    public function stateRankingTable(Request $request){

        $rankings = StateRankings::where('year', $request->year)->where('num_rank', '>', 0)->orderBy('num_rank', 'ASC')->get();
        $row = array("data" => []);
        foreach ($rankings as $ranking) {
            
            $newrow = [
                "percent_rank" => $ranking->percent_rank,
                "num_rank" => $ranking->num_rank,
                "area_rank" => $ranking->area_rank,
                "state" => "<a href='/state/profile/" . $ranking->state_abbr . "' target='_blank'>" . $ranking->state . "</a>",
                "bridges" => number_format($ranking->num_bridges),
                "num_deficient" => number_format($ranking->num_deficient),
                "percent_deficient" => number_format($ranking->percent_deficient, 1) . "%",
                "pcnt_area" => number_format($ranking->pcnt_area, 1) . "%"
            ];

            array_push($row["data"], $newrow);
        }
        return json_encode($row);

    }

    public function topBridgesTable(Request $request){
        $bridges = TopBridges::where('year', $request->year)->get();
        
        $row = array("data" => []);
        foreach ($bridges as $bridge) {
            $state = StateText::where('stname', $bridge->stname)->first();
            if(!is_object($state)){
                $state = StateText::where('stname', $bridge->countyname)->first();
            }
            $newrow = [
            "rank" => $bridge->rank,
            "state" => "<a href='/state/profile/" . $state->state_abbr . "' target='_blank'>" . $state->stname . "</a>",
            "county" => $bridge->countyname,
            "built" => $bridge->built,
            "adt" => number_format($bridge->adt),
            "type" => $bridge->type,
            "location" => $bridge->location
            ];

            array_push($row["data"], $newrow);
        }

        return json_encode($row);
    }

    public function renamePDFS(){
        

        //$this->rekeyPDFs();

        

        
        $dir = public_path() . "/reports";
        $newdir = public_path() . "/reports/congressional/";
        
        $files = array_filter(scandir($newdir), function($item) {
            return !is_dir(public_path() . "/reports/congressional/" . $item);
        });

        natsort($files);

        $files = array_values($files);

        $csv = $dir . "/data_CongressionalBridgeProfiles.csv";
        $newdir = public_path() . "/reports/congressional/";

        $line = array_map('str_getcsv', file($csv));
        $i = 0;
        //dd($files);
        foreach($line as $row){
            if($i > 0 && $files[$i] != ".DS_Store"){
                $filename = $files[$i];
                $newName = "ARTBA_CD_Bridge_Profile_2020_" . $row[2] . "_" . $row[3] . ".pdf";
                echo "job $i: " . $filename . " renamed to " . $newName . "<br>";
                
                rename($newdir .  $filename, $newdir .  $newName);
                
            }
            $i++;
            
            
        }
        
        
    }

    public function rekeyPDFs(){
        $dir = public_path() . "/reports";
        $newdir = public_path() . "/reports/congressional/";


        $files = array_filter(scandir($newdir), function($item) {
            return !is_dir(public_path() . "/reports/congressional/" . $item);
        });

        natsort($files);

        $i = 0;
        foreach ($files as $file) {
            if($i > 0){
                $fileStep = explode("Part", $file);
                rename($newdir . "$file", $newdir . str_replace(".pdf", "", $fileStep[1]) . "_ARTBA_CD_Bridge_Profile_2020.pdf");
            }
            $i++;
        }
    }

    public function exportPDF(Request $request){
        try
        {
            // create the API client instance

            $client = new \Pdfcrowd\HtmlToPdfClient("artba", env('PDF_EXPORT_API_KEY'));

            $state = $request->state;

            $stateRanking = StateRankings::where('state_abbr', $state)->first();

            $stname = $stateRanking->state;

            // custom JS to move elements around

            $client->setOnLoadJavascript("$('#right-aside').appendTo('#right-aside-exported')");

            $client->setCustomCss("
                body,html { background:white !important; margin-top:-50px;}
                .full-width-container { }
                .dynamic-cards { display: flex; flex-direction:row !important; margin-top:100px;}
                .dynamic-cards .col-md-6 { }
                .inner-table { max-height: 1050px !important;}
                .panel-title { margin:50px auto; color:black !important }
                .table-striped .row1 { min-height:80px }
                .pdfcrowd-remove { display:none; width:0 !important; height 0 !important}
                .navbar-brand-pdf { display:block !important}
                .navbar-header, #right-aside { margin-top:-100px; }
                .desktop-padding ul li { font-size:13px; }
                .desktop-padding ul, .chart-container { margin-top:-50px; }
                .right-content { background:white !important }
                .card-silver { background:#FAFAFA !important; border-radius:20px;}
                ");

            $urlToExport = "https://artbabridgereport.org/state/profile/$state";
            $filename =  "/exports/ARTBA " . env('CURRENT_YEAR') . " Bridge Report - $stname.pdf";

            if(isset($request->district)){
                $urlToExport = "https://artbabridgereport.org/congressional/district/$state/" . $request->district;
                $filename = "/exports/ARTBA " . env('CURRENT_YEAR') . " Bridge Report - $stname - District " . $request->district . ".pdf";
            }
            // run the conversion and write the result to a file
            
            $client->convertUrlToFile($urlToExport, public_path() . $filename);

            return Redirect::to($filename);

            
        }
        catch(\Pdfcrowd\Error $why)
        {
            dd($why);
            error_log("Pdfcrowd Error: {$why}\n");
            throw $why;
        }
    }
}
