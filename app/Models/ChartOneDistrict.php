<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChartOneDistrict extends Model
{
    protected $table = "chart_one_cd";
}