<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChartTwoDistrict extends Model
{
    protected $table = "chart_two_cd";
}