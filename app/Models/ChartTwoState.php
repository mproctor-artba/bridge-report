<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChartTwoState extends Model
{
    protected $table = "chart_two_state";
}