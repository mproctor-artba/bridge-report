<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistrictText extends Model
{
    protected $table = "cd_text";
}
