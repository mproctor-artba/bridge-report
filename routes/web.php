<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index')->name('index');


Route::get('/about','PublicController@about')->name('about');
Route::get('/contact', 'PublicController@contact')->name('contact');


Route::prefix('economic-reports')->group(function () {
	Route::get('/','PublicController@reports')->name('reports');
	Route::get('/export-pdf','PublicController@exportPDF')->name('export-pdf');
});

Route::get('/reports/memorial-bridge','PublicController@memorialBridge')->name('memorial-bridge');
Route::get('/news','PublicController@news')->name('news');


Route::get('/state/map', 'PublicController@stateMap')->name('state-map');
Route::get('/state/ranking', 'PublicController@stateRanking')->name('state-ranking');
Route::get('/state/ranking/top-bridges', 'PublicController@topBridges')->name('top-bridges');

Route::get('/state/profile/{state}', 'PublicController@stateProfile')->name('state-profile');

Route::get('/congressional/map', 'PublicController@congressionalMap')->name('congressional-map');
Route::get('/congressional/district/{state}/{district}', 'PublicController@districtProfile')->name('congressional-profile');
// for AJAX request to grab district
Route::get('congressional/reps/{method}/{query}', 'PublicController@queryDistricts');
Route::post('/state/rankings', 'PublicController@stateRankingTable');
Route::post('/state/ranking/top-bridges', 'PublicController@topBridgesTable');

Route::get('/admin/renamePDFS', 'PublicController@renamePDFS');

