jQuery(document).ready(function ($) {
    
var $window = $(window);
checkActivePage();
    
$(".tile").each(function(){
	var width = $(this).width();
	$(this).css('height', width);
});



$(".menu-item-has-children .sub-menu").each(function(){
    $(this).hide();
});

$(".menu-item-has-children").on('click', function (e) {
    var sender = e.target;
    if($(sender).hasClass("menu-item-has-children")){
        $(this).toggleClass("opened");
        $(this).find(".sub-menu").toggle();
    }
    e.stopPropagation();
});

$(".contact-info-container .email").each(function(){
    var link = $(this).find("a").detach();
    $(".contact-info-container .email").hide();
   
    $(".contact-info-container:first").append(link);
     $(".contact-info-container:first").append("<br>");
});

// these scripts will create menu for mobile users
if(isScreenMobile($window)){
    var toggleClassID = Math.floor((Math.random() * 1000000) + 1);
    
    $("#breadcrumbs").after('<div class="accordian fusion-accordian mobile-widget-menu"><div class="panel-group" id="accordion-' + toggleClassID + '"><div class="fusion-panel panel-default"><div class="panel-heading"><h4 class="panel-title toggle"><a data-toggle="collapse" data-parent="#accordion-' + toggleClassID + '" data-target="#' + toggleClassID + 'e" href="#' + toggleClassID + 'e" class="collapsed" aria-expanded="false"><div class="fusion-toggle-icon-wrapper"><i class="fa-fusion-box"></i></div><div class="fusion-toggle-heading">Menu</div></a></h4></div><div id="' + toggleClassID + 'e" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;"><div class="panel-body toggle-content"></div></div></div></div></div>');
    
    toggleClassID = Math.floor((Math.random() * 1000000) + 1);
    
    $(".fusion_builder_column_2_3").append('<div class="accordian fusion-accordian mobile-widget-menu"><div class="panel-group" id="accordion-' + toggleClassID + '"><div class="fusion-panel panel-default"><div class="panel-heading"><h4 class="panel-title toggle"><a data-toggle="collapse" data-parent="#accordion-' + toggleClassID + '" data-target="#' + toggleClassID + 'e" href="#' + toggleClassID + 'e" class="collapsed" aria-expanded="false"><div class="fusion-toggle-icon-wrapper"><i class="fa-fusion-box"></i></div><div class="fusion-toggle-heading">Menu</div></a></h4></div><div id="' + toggleClassID + 'e" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;"><div class="panel-body toggle-content"></div></div></div></div></div>');
    
    $(".widget_nav_menu").delay(1000).detach().appendTo('.toggle-content');
    $(".contact_info").delay(1000).detach().appendTo('.toggle-content');
    $(".menu-item-has-children").find(".sub-menu").toggle();
}

$(".menu li").each(function(){
        var url      = window.location.href;     // Returns full URL
       if($(this).hasClass("current-menu-item") || $(this).hasClass("current-menu-parent")) {

           $(this).toggleClass("opened");
            $(this).find(".sub-menu").toggle();
       }
    });

// Configure/customize these variables.

$(".showMore .title").each(function(){
    var title = $(this).text();
    $(this).parent("div").find(".image").append("<br>" + title);
    $(this).text("");
});



});

        jQuery(document).ready(function ($) {
          var trigger = $('.hamburger'),
              overlay = $('.overlay'),
             isClosed = false;
        
            trigger.click(function () {
              hamburger_cross();      
            });

            function hamburger_cross() {
              if (isClosed == true) {          
                overlay.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
                
              } else {   
                overlay.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
              }
          }
          
          $(".overlay").click(function(){
            $('#wrapper').toggleClass('toggled');
            hamburger_cross();
        });
  
        $('[data-toggle="offcanvas"]').click(function () {
            $('#wrapper').toggleClass('toggled');
        });
        
        $('body').on("click", ".navbar-toggle", function () {
           $(".primarymenu").toggleClass('collapse');
            console.log("show" );
        });
        
        var breadcrumbs = $(".fusion-breadcrumbs").html();
        $("#breadcrumbs").html(breadcrumbs);

        
        $(".showMore .excerpt").each(function(){
            var text = $(this).text();
            wordCount = text.replace( /[^\w ]/g, "" ).split( /\s+/ ).length
            maxWords = 51;
            
            if(wordCount > maxWords){
                console.log(wordCount);
                var lastWords = text.split(/\s+/).slice(maxWords,10000).join(" ");
                var split = text.split(/\s+/).slice(0,maxWords).join(" ");
                $(this).empty();
                $(this).html(split + "<span class='fullText'> " + lastWords + "</span><a href='javascript:;' class='btn getFullText' style='padding-left:5px;'>...Read More</a>");
            }
        });
        
        $("body").on("click",".getFullText",function(){
            
            $(this).parent().find(".fullText").toggle();
        });
        
        $("body").on("click", ".getFullText", function () {
            console.log("clicked");
            $(this).text(function(i, v){
               return v === '...Read More' ? '  Show Less' : '...Read More'
            })
        });
        
        $("#safetytimeline .listing-item .image").each(function(){
            
            var image = $(this).html();
            $(this).parent().find(".excerpt").prepend(image);
            $(this).remove();
        });
        
        $(document).tooltip({
        items: '.fusion-tooltip, .tooltip-shortcode',
        track: true,
        show: false,
        selector: '.fusion-tooltip, .tooltip-shortcode',
        hide: false
});
});
        
        
            
        
            
            setTimeout(function(){
        var height = jQuery(".tile").width();
            jQuery(".tile").each(function(){
            jQuery(this).css('height', height);
        });
        }, 100);
            
            
            /*Mission Statment*/
            
   
        jQuery(window).resize(function() {
            sizeTiles();
        }).resize()

        function sizeTiles(){
            setTimeout(function(){
                var height = jQuery(".tile").width();
                jQuery(".tile").each(function(){
                    jQuery(this).css('height', height);
                });
            }, 100);
        }
             /*Mission Statment*/

function isScreenMobile(window) {
        var windowsize = window.width();
        if (windowsize <= 1024) {
            return true;
        }
        return false;
    }
    
function checkActivePage(){
    
}

$('.numbercounter').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 1000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
